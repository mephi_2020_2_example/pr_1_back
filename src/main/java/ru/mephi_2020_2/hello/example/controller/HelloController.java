package ru.mephi_2020_2.hello.example.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.mephi_2020_2.hello.example.model.HelloInformation;
import ru.mephi_2020_2.hello.example.services.HelloServices;

@Controller
@RequestMapping("/api/v1/hello")
public class HelloController {

    public final static Logger logger = LoggerFactory.getLogger(HelloController.class);

    @Autowired
    HelloServices helloServices;

    @GetMapping
    @ResponseBody
    public HelloInformation get() {
        HelloInformation helloInformation = helloServices.get();
        logger.debug("get resp {}", helloInformation);
        return helloInformation;
    }
}
