package ru.mephi_2020_2.hello.example.controller;

import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.mephi_2020_2.hello.example.services.RandServices;

@Controller
@RequestMapping("/api/v1/rand")
public class RandController {
    public final static Logger logger = LoggerFactory.getLogger(RandController.class);

    @Autowired
    RandServices randServices;

    @GetMapping
    @ResponseBody
    public String get() {
        logger.debug("get");
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.put("number", randServices.getRandomInt());
            jsonObject.put("success", true);
        } catch (Exception e) {
            logger.error(null, e);
            jsonObject.put("success", false);
            jsonObject.put("error", e.getMessage());
        }
        return jsonObject.encode();
    }
}
