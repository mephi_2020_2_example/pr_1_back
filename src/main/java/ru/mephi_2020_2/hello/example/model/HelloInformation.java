package ru.mephi_2020_2.hello.example.model;

import io.vertx.core.json.JsonObject;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

/**
 * Информация для HelloCntroller
 */
public class HelloInformation {

    public enum PartOfDay {
        MORNING(1),
        DAY(2),
        EVENING(3),
        NIGHT(4);

        private int value;

        PartOfDay(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * Текст приветствия
     */
    private String text;
    /**
     * Текущее время
     */
    private LocalTime localTime;
    /**
     * Часть дня
     */
    private PartOfDay partOfDay;

    public HelloInformation() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalTime getLocalTime() {
        return localTime;
    }

    public void setLocalTime(LocalTime localTime) {
        this.localTime = localTime;
    }

    public PartOfDay getPartOfDay() {
        return partOfDay;
    }

    public void setPartOfDay(PartOfDay partOfDay) {
        this.partOfDay = partOfDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HelloInformation that = (HelloInformation) o;
        return Objects.equals(text, that.text) &&
                Objects.equals(localTime, that.localTime) &&
                partOfDay == that.partOfDay;
    }

    @Override
    public int hashCode() {
        return Objects.hash(text, localTime, partOfDay);
    }

    @Override
    public String toString() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("text", text);
        jsonObject.put("localtime", localTime.format(DateTimeFormatter.ISO_TIME));
        jsonObject.put("partOfDay", partOfDay.getValue());
        return jsonObject.encode();
    }
}
