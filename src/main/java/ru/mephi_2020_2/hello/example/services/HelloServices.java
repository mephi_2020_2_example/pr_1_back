package ru.mephi_2020_2.hello.example.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.mephi_2020_2.hello.example.model.HelloInformation;

import java.time.LocalTime;

@Service
public class HelloServices {

    public final static Logger logger = LoggerFactory.getLogger(HelloServices.class);

    public HelloInformation get() {
        logger.debug("get");
        HelloInformation helloInformation = new HelloInformation();
        LocalTime localTime = LocalTime.now();

        if (localTime.getHour() < 5) {
            helloInformation.setPartOfDay(HelloInformation.PartOfDay.NIGHT);
            helloInformation.setText("Доброй ночи");
        } else if (localTime.getHour() < 11) {
            helloInformation.setPartOfDay(HelloInformation.PartOfDay.MORNING);
            helloInformation.setText("Доброе утро");
        } else if (localTime.getHour() < 16) {
            helloInformation.setPartOfDay(HelloInformation.PartOfDay.DAY);
            helloInformation.setText("Доброй день");
        } else if (localTime.getHour() < 22) {
            helloInformation.setPartOfDay(HelloInformation.PartOfDay.EVENING);
            helloInformation.setText("Доброй вечер");
        } else {
            helloInformation.setPartOfDay(HelloInformation.PartOfDay.NIGHT);
            helloInformation.setText("Доброй ночи");
        }
        helloInformation.setLocalTime(localTime);
        return helloInformation;
    }
}
