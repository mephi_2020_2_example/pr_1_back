package ru.mephi_2020_2.hello.example.services;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class RandServices {

    private final Random random;

    public RandServices() {
        random = new Random();
    }

    public Random getRandom() {
        return random;
    }

    public int getRandomInt() {
        return random.nextInt();
    }
}
